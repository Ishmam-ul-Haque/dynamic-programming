#include<bits/stdc++.h>
using namespace std;

#define mx 1007

int dp[mx];

/* Returns the best obtainable price for a rod of length n and
   price[] as prices of different pieces */
int cutRod(int price[], int n)
{
    if (n <= 0)
        return 0;
    if(dp[n]!=-1) return dp[n];
    int highest = INT_MIN;

    // Recursively cut the rod in different pieces and compare different
    // configurations
    for (int i = 0; i<n; i++)
        highest = max(highest, price[i] + cutRod(price, n-i-1));

    return dp[n] = highest;
}

int main()
{
    memset(dp,-1,sizeof dp);
    int arr[] = {1, 5, 8, 9, 10, 17, 17, 20};
    int len = sizeof(arr)/sizeof(arr[0]);
    int ans = cutRod(arr, len);
    cout<<"Maximum Obtainable Value is : "<<ans<<endl;
    return 0;
}
